import numpy as np
import sys
#
dbg=1
dim=3
if dim==3:
   #True sequence of blocks for 3x3x3 snake puzzle
   seq = np.array([3,2,2,2,1,1,1,2,2,1,1,2,1,2,1,1,2], np.int8)
   #Solves it in a fraction of a second and finds the following solution
   #
   #           dir1           
   #            /\            
   #            /             
   #           /              
   #         03-----03-----02 
   #        / |     /|     /| 
   #       12-----13-----02 | 
   #      / | |   /  |   /| | 
   #    01-----01-----01------------> dir3
   #     |  |04-|---06-|--|07 
   #     |  |/| |   /| |  |/| 
   #     | 12---|-14---|-08 | 
   #     | /| | | /  | | /| | 
   #    09-----09-----08  | | 
   #     |  |04-|---05-|--|17 
   #     |  |/  |   /  |  |/  
   #     | 11---|-14---|-17   
   #     | /    | /    | /    
   #    10-----15-----16      
   #     |                    
   #     |                    
   #     \/                   
   #    dir2                  
elif dim==4:
   #True sequence of blocks for 4x4x4 snake puzzle
   seq = np.array([3,1,2,1,1,3,1,2,1,2,1,2,1,1,1,1,1,1,1,1,2,2,1,1,1,1,1,2,3,1,1,1,3,1,2,1,1,1,1,1,1,1,1,1,3,1], np.int8)
   #Solves it in about 30 minutes, much longer than the 3x3x3 version!!
   #   [[[ 1  1  1  7]
   #     [ 5  6  6  6]
   #     [30 31 41 42]
   #     [29 32 44 43]]
   #   
   #    [[ 3  3  2  8]
   #     [ 4 22 22 21]
   #     [24 23 40 39]
   #     [29 33 45 38]]
   #   
   #    [[13 12  9  8]
   #     [26 12 10 21]    26
   #     [25 11 10 36]
   #     [29 33 45 37]]
   #   
   #    [[14 15 18 19]
   #     [27 16 17 20]
   #     [28 34 35 35]
   #     [28 33 45 46]]]
#=======================================================================
def testAllDir(cube,idx,seq,iseq,dir0):
   #Input:
   #   cube[dim,dim,dim] = puzzle size
   #   idx  = [i1,i2,i3] = last position that was filled in cube
   #          0 <= i1/2/3 < dim
   #   seq  = full sequence of blocks
   #   seq[iseq] = next sequence to add
   #   dir0 = last direction that was used
   #
   #Output:
   #   Returns 1 if successful
   #   Returns 0 if error, not able to add sequence
   #
   if iseq==len(seq):
      print('BINGO')
      return 1
   elif iseq==0:
      d = np.nditer(np.array([3]))
      dir0=2
   else:
      d = np.nditer(np.array([1,-1,2,-2,3,-3]))
   if dbg:
      print('%6.2f%%, iseq %3d/%3d, seq=%d, dir0=%d starting at idx=<%d,%d,%d>' % ((iseq+1)/len(seq)*100,iseq+1,len(seq),seq[iseq],dir0,idx[0],idx[1],idx[2]))
   while not d.finished:
      #If not the same as previous orientation
      if dbg>2:
         print('- dir0 (%d) --> dir1 (%d)' % (dir0,d[0]))
      if not abs(d[0])==abs(dir0):
         idx1  = idx.copy()
         #If adding next sequence was successful
         if addseq(cube,idx1,seq[iseq],d[0],iseq+1):
            #If this was the last sequence to add, BINGO!
            if iseq==len(seq):
               return 1
            #Otherwise, we'll test the next sequence
            dir1=d[0]
            iseq+=1
            #If adding all the sequences to the end was successful
            #quick return
            if testAllDir(cube,idx1,seq,iseq,dir1):
               return 1
            else:
               #Error, not able to add sequence
               if dbg>2:
                  print('All direction filled')
               iseq-=1
               #idx = idx1
               #If anything went wrong, clean-up the matrix
               clearAll(cube,iseq+1)
               #printCube(cube)
               if dbg:
                  print('%6.2f%%, iseq %3d/%3d, seq=%d, dir0=%d starting at idx=<%d,%d,%d>' % ((iseq+1)/len(seq)*100,iseq+1,len(seq),seq[iseq],dir0,idx[0],idx[1],idx[2]))
      elif dbg>2:
         print("    Skipping direction d[0]=%d, same as dir0=%d" % (d[0],dir0))
      d.iternext()
   return 0
#
#=======================================================================
def addseq(cube,idx,seqlen,dir1,ival):
   #Input:
   #   cube[dim,dim,dim] = puzzle size
   #   idx  = [i1,i2,i3] = last position that was filled in cube
   #          0 <= i1/2/3 < dim
   #   seqlen = length of the sequence to add
   #   dir1   = direction of the length to add [-]1/2/3)
   #   ival   = value to add in each of the term of the sequence
   #
   #Output:
   #   Returns 1 if successful
   #   Returns 0 if error, not able to add sequence
   #
   if dbg>2:
      print('    adding %d times %d starting from idx=<%d,%d,%d>' % (seqlen,ival,idx[0],idx[1],idx[2]))
   idx2=idx.copy()
   #Array slicing (start:stop:step)
   if dir1==1:
      vect = cube[idx2[0]+1::,idx2[1]    ,idx2[2]]
      idx2[0]+=seqlen
   elif dir1==-1:
      vect = cube[:idx2[0]:  ,idx2[1]    ,idx2[2]]
      idx2[0]-=seqlen
   elif dir1==2:
      vect = cube[idx2[0]    ,idx2[1]+1::,idx2[2]]
      idx2[1]+=seqlen
   elif dir1==-2:
      vect = cube[idx2[0]    ,:idx2[1]:  ,idx2[2]]
      idx2[1]-=seqlen
   elif dir1==3:
      vect = cube[idx2[0]    ,idx2[1]    ,idx2[2]+1::]
      idx2[2]+=seqlen
   elif dir1==-3:
      vect = cube[idx2[0]    ,idx2[1]    ,:idx2[2]:]
      idx2[2]-=seqlen
   else:
      print('      Error, %d is not a valid direction' % dir1)
      return 0
   #print('      len,vect=%d,' % len(vect),vect)
   #If the length is not long enough, error
   if len(vect)<seqlen:
      if dbg>2:
         print('      Error, sequence length (%d) too long for given space (%d)' % (seqlen,len(vect)))
      return 0
   elif len(vect)==seqlen:
      vectLen = vect
   else:
      if dir1>0:
         vectLen = vect[0:seqlen]
      else:
         vectLen = vect[-1:len(vect)-seqlen-1:-1]
   #If any of the elements is not 0, error
   if sum(vectLen)>0:
      if dbg>2:
         print('      Error, space already filled',vectLen)
      return 0
   vectLen+=ival
   idx[0]=idx2[0]
   idx[1]=idx2[1]
   idx[2]=idx2[2]
   #print('    New end position idx=<%d,%d,%d>' % (idx[0],idx[1],idx[2]))
   if dbg:
      printCube(cube)
   #print('   ',cube.tolist())
   return 1
#
#=======================================================================
#Reset to 0 all numbers >= iseq
def clearAll(cube,iseq):
   it = np.nditer(cube, op_flags=['readwrite'])
   while not it.finished:
      if it[0]>=iseq:
         it[0]=0
      it.iternext()
   if dbg>2:
      print('      clearAll iseq=%d' % iseq)
      printCube(cube)
#=======================================================================
def printCube(c):
   if dbg>=2:
      if dim==3:
         print('           dir1           ')
         print('            /\            ')
         print('            /             ')
         print('           /              ')
         print('         %02d-----%02d-----%02d ' % (c[2,0,0],c[2,0,1],c[2,0,2]))
         print('        / |     /|     /| ')
         print('       %02d-----%02d-----%02d | ' % (c[1,0,0],c[1,0,1],c[1,0,2]))
         print('      / | |   /  |   /| | ')
         print('    %02d-----%02d-----%02d------------> dir3' % (c[0,0,0],c[0,0,1],c[0,0,2]))
         print('     |  |%02d-|---%02d-|--|%02d ' % (c[2,1,0],c[2,1,1],c[2,1,2]))
         print('     |  |/| |   /| |  |/| ')
         print('     | %02d---|-%02d---|-%02d | ' % (c[1,1,0],c[1,1,1],c[1,1,2]))
         print('     | /| | | /  | | /| | ')
         print('    %02d-----%02d-----%02d  | | ' % (c[0,1,0],c[0,1,1],c[0,1,2]))
         print('     |  |%02d-|---%02d-|--|%02d ' % (c[2,2,0],c[2,2,1],c[2,2,2]))
         print('     |  |/  |   /  |  |/  ')
         print('     | %02d---|-%02d---|-%02d   ' % (c[1,2,0],c[1,2,1],c[1,2,2]))
         print('     | /    | /    | /    ')
         print('    %02d-----%02d-----%02d      ' % (c[0,2,0],c[0,2,1],c[0,2,2]))
         print('     |                    ')
         print('     |                    ')
         print('     \/                   ')
         print('    dir2                  ')
      else:
         print(cube)
#
#======================================================================#
#                          MAIN PROGRAM                                #
#======================================================================#
#
#
#Debug cube with numbers 0 to 26
#cube = np.arange(27).reshape(3,3,3)
#dir0=3
#addseq(cube,[0,0,-1],3,dir0,7)
#print(cube)
#clearAll(cube,7)
#print(cube)
#printCube(cube)
#vect = np.array([1,2,3,4,5])
#seqlen=2
#vectLen = vect[-1:len(vect)-seqlen-1:-1]
#print(vectLen)
#sys.exit()
#           dir1
#            /\
#            /
#           /
#         18-----19-----20
#        / |     /|     /|
#        9-----10-----11 |
#      / | |   /  |   /| |
#     0------1------2------------> dir3
#     |  |21-|---22-|--|23
#     |  |/| |   /| |  |/|
#     | 12---|-13---|-14 |
#     | /| | | /  | | /| |
#     3------4------5  | |
#     |  |24-|---25-|--|26
#     |  |/  |   /  |  |/
#     | 15---|-16---|-17
#     | /    | /    | /
#     6------7------8
#     |
#     |
#     \/
#    dir2
#
cube = np.zeros( (dim, dim, dim), np.int8 )
#Debug sequence
#seq = np.array([2,2,2,2,1,1,1,2,2,1,1,2,1,2,1,1,3], np.int8)
if dbg:
   print(seq)
if sum(seq)-dim**3:
   print("Error, the sum of all pieces, %d\ndoes not match with the dimension of the cube %s**3=%s\n" % (sum(seq), dim, dim**3))
else:
   print("OK, the sum of all pieces, %d\nmatches with the dimension of the cube %s**3=%s\n" % (sum(seq), dim, dim**3))
#
#Sequence ID to start with
iseq=0
#Current orientation (always start in Z)
#(see "d = np.nditer([3])" in testAllDir)
dir0=0
#################################################################
############## TEST with only one starting position #############
#################################################################
#p = np.array([0,2,-1], np.int8)
#testAllDir(cube,p,seq,iseq,dir0)
#sys.exit()
#################################################################
################### Loop on all staring positions ###############
#################################################################
#Iterator on the starting positions
itstart = np.nditer(cube[0:dim,0:dim,0:dim-seq[0]+1], flags=['multi_index'])
while not itstart.finished:
   p = np.array([itstart.multi_index[0],
                 itstart.multi_index[1],
                 itstart.multi_index[2]-1], np.int8)
   #If successful addition of the sequence, quit
   if testAllDir(cube,p,seq,iseq,dir0):
      break
   itstart.iternext()
dbg=2
printCube(cube)
